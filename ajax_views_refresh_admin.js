
/**
 * @file
 * Handle checkboxes on ajax_views_refresh admin form.
 */

Drupal.behaviors.ajax_views_refresh = function() {
	$('#ajax-views-refresh-form :checkbox').change(function() {
		var element = $(this);
		var val = element.val();
		// Children
		if (val.match("\:")) {
			var viewName = val.substring(0, val.indexOf(':'));
			// Uncheck parent if a child is unchecked
			if (!element.attr('checked')) {
				$('input[value='+viewName+']', element.parents('.form-checkboxes')).attr('checked', false);
			}
			// Check parent if all children are checked
			else {
				var allChecked = true;
				$('input[value^='+viewName+':]', element.parents('.form-checkboxes')).each(function() {
					if (!$(this).attr('checked')) {
						allChecked = false;
					}
				});
				if (allChecked) {
					$('input[value='+viewName+']', element.parents('.form-checkboxes')).attr('checked', true);
				}
			}
		}
		// Parent
		else {
			// Check all children
			if (element.attr('checked')) {
				$('input[value^='+val+':]', element.parents('.form-checkboxes')).attr('checked', true);
			}
			// Uncheck all children
			else {
				$('input[value^='+val+':]', element.parents('.form-checkboxes')).attr('checked', false);
			}
		}
	});
}