
Ajax Views refresh
------------------
Allows views to be refreshed on Ajax forms submission.
The best use case is a node creation form and a view of nodes below, which would be refreshed each time you add or edit a node.

Installation
------------
Install and enable the following modules : Ajax, Ajax ui, Ajax plugin - disable_redirect, Views, Views ui, and of course Ajax plugin - views_refresh.
Go to admin/settings/ajax, and check "Enabled", "Disable Redirect" and "Refresh views on submit" for at least one form (probably a node type creation form, e.g. "Story").
Go to admin/build/views/add, and create a view, sorted by Post date, descending. Add a Block display.
IMPORTANT : Set "Use AJAX" to "Yes" for this display or it won't work.
Go to admin/build/block, and add the previously created block to a region. You could set the block visibility to the node creation form.
Go to node/add, and pick the content type for which you enabled Ajax. On form submission, the view is updated !

Maintainers
-----------
Julien De Luca